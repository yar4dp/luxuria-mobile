package id.faizal.hotelxyz.feature.authenticate.register

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.DatePicker
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.model.ResponseModel
import id.faizal.hotelxyz.model.TokenModel
import id.faizal.hotelxyz.network.apiService
import id.faizal.hotelxyz.network.serviceDateFormatPattern
import id.faizal.hotelxyz.utilities.*
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class RegisterActivity : AppCompatActivity(), Callback<ResponseModel<TokenModel>>, Customer.GetProfileInterface {

    var birthday = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        registerButton.setOnClickListener {
            if (registerValid()) {
                register()
            }
        }

        inputBirthday.setOnClickListener {
            showDatePicker(this, birthday, object: DatePickerDialog.OnDateSetListener {
                override fun onDateSet(p0: DatePicker?, y: Int, m: Int, d: Int) {
                    birthday.set(Calendar.YEAR, y)
                    birthday.set(Calendar.MONTH, m)
                    birthday.set(Calendar.DAY_OF_MONTH, d)
                    inputBirthday.setText(formatCalendar(birthday))
                }
            })
        }
    }

    private fun registerValid() : Boolean {
        if (inputEmail.text.isEmpty()) {
            showSnackbar(window.decorView, "Please input your email.")
            return false
        }
        if (inputPassword.text.isEmpty()) {
            showSnackbar(window.decorView, "Please input password.")
            return false
        }
        if (inputFirstName.text.isEmpty()) {
            showSnackbar(window.decorView, "Please input first name.")
            return false
        }
        if (inputLastName.text.isEmpty()) {
            showSnackbar(window.decorView, "Please input last name.")
            return false
        }
        if (inputAddress.text.isEmpty()) {
            showSnackbar(window.decorView, "Please input your address.")
            return false
        }
        if (inputBirthday.text.isEmpty()) {
            showSnackbar(window.decorView, "Please input your birthday.")
            return false
        }
        if (inputPhoneNumber.text.isEmpty()) {
            showSnackbar(window.decorView, "Please input your phone number.")
            return false
        }
        return true
    }

    private fun register() {
        apiService.customerRegistration(
                inputEmail.text.toString(),
                inputPassword.text.toString(),
                inputFirstName.text.toString(),
                inputLastName.text.toString(),
                formatCalendar(birthday, serviceDateFormatPattern),
                inputAddress.text.toString(),
                inputPhoneNumber.text.toString()
        ).enqueue(this)
    }

    override fun onResponse(call: Call<ResponseModel<TokenModel>>?, response: Response<ResponseModel<TokenModel>>) {
        if (response.body()?.status == "200") {
            Customer.login(this, response.body()!!.data, this)
        } else {
            showSnackbar(window.decorView, response.body()!!.message)
        }
    }

    override fun onFailure(call: Call<ResponseModel<TokenModel>>?, t: Throwable) {
        showSnackbar(window.decorView, t.localizedMessage)
    }

    private fun registerSuccess() {
        AlertDialog.Builder(this)
                .setTitle("Congratulation!")
                .setMessage("Welcome, " + Customer.profile?.fullName() + "!\nYour account has been registered.")
                .setPositiveButton("Home", object: DialogInterface.OnClickListener {
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        showDashboard(this@RegisterActivity)
                        finishAffinity()
                    }
                })
                .setCancelable(false)
                .show()
    }

    override fun profileSuccessRetrieved() {
        registerSuccess()
    }

    override fun profileFailedToRetrievedBecause(message: String) {
        showSnackbar(window.decorView, message)
    }

}
