package id.faizal.hotelxyz.feature.main

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.feature.about.AboutActivity
import id.faizal.hotelxyz.feature.authenticate.login.LoginActivity
import id.faizal.hotelxyz.feature.contact.ContactActivity
import id.faizal.hotelxyz.feature.reservation.list.ListReservationActivity
import id.faizal.hotelxyz.utilities.Customer
import id.faizal.hotelxyz.utilities.showSnackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        content.adapter = MainActivityFragmentPagerAdapter(supportFragmentManager)
        tabIndicator.setupWithViewPager(content, true)

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout

        menuButton.setOnClickListener {
            drawer.openDrawer(GravityCompat.START)
        }

        aboutUsButton.setOnClickListener {
            startActivity(Intent(this, AboutActivity::class.java))
        }

        contactUsButton.setOnClickListener {
            startActivity(Intent(this, ContactActivity::class.java))
        }

        loginButton.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        logoutButton.setOnClickListener {
            askUserToLogout()
        }

        if (Customer.hasBeenLoggedIn(this)) {
            showSnackbar(window.decorView, "Welcome back " + Customer.profile?.fullName() + "!")
        }

        listReservationButton.setOnClickListener {
            showCustomerReservationList()
        }
    }

    override fun onResume() {
        super.onResume()
        if (Customer.hasBeenLoggedIn(this)) {
            loginButton.visibility = View.GONE
            listReservationButton.visibility = View.VISIBLE
            logoutButton.visibility = View.VISIBLE
        } else {
            loginButton.visibility = View.VISIBLE
            listReservationButton.visibility =  View.GONE
            logoutButton.visibility = View.GONE
        }
    }

    inner class MainActivityFragmentPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return MainFragment1()
                1 -> return MainFragment2()
                else -> return MainFragment3()
            }
        }

        override fun getCount(): Int {
            return 3
        }

    }

    private fun askUserToLogout() {
        AlertDialog.Builder(this)
                .setTitle("Confirm Logout")
                .setMessage("Are you sure want to logout?")
                .setPositiveButton("Yes", object: DialogInterface.OnClickListener {
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        logoutUser()
                    }
                })
                .setNegativeButton("Cancel", null)
                .setCancelable(true)
                .show()
    }

    private fun logoutUser() {
        Customer.logout(this)
        startActivity(Intent(this@MainActivity, MainActivity::class.java))
        finishAffinity()
    }

    private fun showCustomerReservationList() {
        startActivity(Intent(this, ListReservationActivity::class.java))
    }

}
