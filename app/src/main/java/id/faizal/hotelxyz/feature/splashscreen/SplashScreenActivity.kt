package id.faizal.hotelxyz.feature.splashscreen

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.utilities.Customer
import id.faizal.hotelxyz.utilities.showDashboard

class SplashScreenActivity : AppCompatActivity(), Customer.GetProfileInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        if (Customer.hasToken(this)) {
            val token = Customer.token(this)
            Customer.refreshProfile(token!!, this)
        } else {
            showDashboard(this)
            finish()
        }
    }

    override fun profileSuccessRetrieved() {
        showDashboard(this)
        finish()
    }

    override fun profileFailedToRetrievedBecause(message: String) {
        showDashboard(this)
        finish()
        Customer.logout(this)
    }

}
