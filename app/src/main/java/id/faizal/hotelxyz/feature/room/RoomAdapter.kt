package id.faizal.hotelxyz.feature.room

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.model.RoomModel

class RoomAdapter (
        val context: Context,
        val list: List<RoomModel>
) : RecyclerView.Adapter<RoomAdapter.ViewHolder>() {

    var clickListener: ItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.showRoomItem(list[position])
    }

    override fun getItemCount(): Int {
        return list.count()
    }


    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var item: RoomModel? = null

        override fun onClick(p0: View?) {
            clickListener?.onItemClicked(item!!)
        }

        fun showRoomItem(item: RoomModel) {
            this.item = item
            val imageView = itemView.findViewById<ImageView>(R.id.imageView)
            val textView = itemView.findViewById<TextView>(R.id.textView)
            textView.text = item.name
            Picasso.with(itemView.context).load(item.imageUrl).into(imageView)
            itemView.setOnClickListener(this)
        }

    }

    fun setItemClickListener(clickListener: ItemClickListener): RoomAdapter {
        this.clickListener = clickListener
        return this
    }

    interface ItemClickListener {
        fun onItemClicked(item: RoomModel)
    }

}