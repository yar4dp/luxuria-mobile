package id.faizal.hotelxyz.model

import com.google.gson.annotations.SerializedName

data class EmployeeModel (

        @SerializedName("user")
        val user: UserModel,

        @SerializedName("first_name")
        val firstName: String,

        @SerializedName("last_name")
        val lastName: String,

        @SerializedName("phone_number")
        val phoneNumber: String

)